WHENEVER SQLERROR EXIT SQL.SQLCODE;
CONNECT SANDBOX/SANDBOX@db/orclsndb.localdomain;

SET TIME ON;
SET ECHO ON;
SET SERVEROUTPUT ON;

ALTER SESSION SET "_ORACLE_SCRIPT"=TRUE;

BEGIN  
    FOR loop_counter IN 1..2500 
    LOOP             
        INSERT INTO uuid_test(idn, ids, idb)
        SELECT 
            uuid_name_seq.NEXTVAL, 
            uuid5(uuid_name_seq.CURRVAL), 
            uuid5b(uuid_name_seq.CURRVAL)
        FROM dual CONNECT BY LEVEL <= 10000;                
        COMMIT; 
    END LOOP;     
END;

/

EXIT;
