CREATE OR REPLACE AND COMPILE JAVA SOURCE NAMED "UUIDFactory" AS

/*
The code in this class has been copied from https://github.com/cowtowncoder/java-uuid-generator/releases/tag/java-uuid-generator-4.0.1
and it is subjected to Apache License Version 2.0 https://raw.githubusercontent.com/cowtowncoder/java-uuid-generator/master/LICENSE.

If you choose to use it, please acknowledge the origina author
*/

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;
import oracle.sql.RAW; 

public class UUIDFactory {

    /**
     * Enumeration of different flavors of UUIDs: 5 specified by specs
     * (<a href="http://tools.ietf.org/html/rfc4122">RFC-4122</a>)
     * and one
     * virtual entry ("UNKNOWN") to represent invalid one that consists of
     * all zero bites
     */
    public enum UUIDType {
        TIME_BASED(1),
        DCE(2),
        NAME_BASED_MD5(3),
        RANDOM_BASED(4),
        NAME_BASED_SHA1(5),
        UNKNOWN(0)
        ;

        private final int _raw;
        
        private UUIDType(int raw) {
        _raw = raw;
        }

        /**
         * Returns "raw" type constants, embedded within UUID bytes.
         */
        public int raw() { return _raw; }
    }


    private final static SecureRandom random = new SecureRandom();

    public final static int BYTE_OFFSET_CLOCK_LO = 0;
    public final static int BYTE_OFFSET_CLOCK_MID = 4;
    public final static int BYTE_OFFSET_CLOCK_HI = 6;

    // note: clock-hi and type occupy same byte (different bits)
    public final static int BYTE_OFFSET_TYPE = 6;

    // similarly, clock sequence and variant are multiplexed
    public final static int BYTE_OFFSET_CLOCK_SEQUENCE = 8;
    public final static int BYTE_OFFSET_VARIATION = 8;

    /**
     * Namespace used when name is a DNS name.
     */
    public final static UUID NAMESPACE_DNS = UUID.fromString("6ba7b810-9dad-11d1-80b4-00c04fd430c8");

    /**
     * Namespace used when name is a URL.
     */
    public final static UUID NAMESPACE_URL = UUID.fromString("6ba7b811-9dad-11d1-80b4-00c04fd430c8");
    /**
     * Namespace used when name is an OID.
     */
    public final static UUID NAMESPACE_OID = UUID.fromString("6ba7b812-9dad-11d1-80b4-00c04fd430c8");
    /**
     * Namespace used when name is an X500 identifier
     */
    public final static UUID NAMESPACE_X500 = UUID.fromString("6ba7b814-9dad-11d1-80b4-00c04fd430c8");
    
    public static final UUID NAMESPACE_PRIVATE = UUID.fromString("a9234504-5747-4821-92f9-9c1fa55fcf4b");

    public final static Charset _utf8;
    public final static MessageDigest _sha1_digester;
    public final static MessageDigest _md5_digester;
    public final static byte[] _namespace;
    static {
        _utf8 = Charset.forName("UTF-8");
        try {
            _sha1_digester = MessageDigest.getInstance("SHA-1");
            _md5_digester = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException nex) {
            throw new IllegalArgumentException("Couldn't instantiate MessageDigest instance: "+nex.toString());
        }
        _namespace = asByteArray(NAMESPACE_PRIVATE);
    }


    /* *****************************************************************************************************  */
    /*                            METHODS EXPOSED AS SQL FUNCTIONS                                            */
    /* *****************************************************************************************************  */
    public static String namespace() {
        return NAMESPACE_PRIVATE.toString();
    }   

    public static String uuid3asString(String name) {
        return uuid5(name).toString();
    }
    
    public static String uuid4asString() {
        return uuid4().toString();
    }

    public static String uuid5asString(String name) {
        return uuid5(name).toString();
    }

    public static RAW uuid3asBytes(String name) {
        return new RAW(asByteArray(uuid3(name)));
    }       

    public static RAW uuid4asBytes() {
        return new RAW(asByteArray(uuid4()));
    }

    public static RAW uuid5asBytes(String name) {
        return new RAW(asByteArray(uuid5(name)));
    }

    public static RAW uuidAsBytes(String uuid) {
        return new RAW(asByteArray(fromString(uuid)));
    }

    public static String uuidFromBytes(RAW uuid) {
        return uuid(uuid.getBytes()).toString();
    }


    /* *****************************************************************************************************  */
    /*                                  INTERNAL IMPLEMENTATION                                               */
    /* *****************************************************************************************************  */

    public static UUID uuid3(String name){
        return nameBasedUUID(name,UUIDType.NAME_BASED_MD5,_md5_digester);     
    }
    
    public static UUID uuid4(){
        long r1 = random.nextLong();
        long r2 = random.nextLong();
        return constructUUID(UUIDType.RANDOM_BASED, r1, r2);
    }

    public static UUID uuid5(String name){
        return nameBasedUUID(name,UUIDType.NAME_BASED_SHA1,_sha1_digester);     
    }


    public static byte[] asByteArray(UUID uuid){
        long hi = uuid.getMostSignificantBits();
        long lo = uuid.getLeastSignificantBits();
        byte[] result = new byte[16];
        _appendInt((int) (hi >> 32), result, 0);
        _appendInt((int) hi, result, 4);
        _appendInt((int) (lo >> 32), result, 8);
        _appendInt((int) lo, result, 12);
        return result;
    }    


   private static UUID nameBasedUUID(String name, UUIDType type, MessageDigest digester){
        byte[] nameBytes = name.getBytes(_utf8);
        byte[] digest;
        synchronized (digester) {
            digester.reset();
            digester.update(_namespace);
            digester.update(nameBytes);
            digest = digester.digest();
        }
        return constructUUID(type, digest);        
    }


    private static UUID constructUUID(UUIDType type, long l1, long l2){
        // first, ensure type is ok
        l1 &= ~0xF000L; // remove high nibble of 6th byte
        l1 |= (long) (type.raw() << 12);
        // second, ensure variant is properly set too (8th byte; most-sig byte of second long)
        l2 = ((l2 << 2) >>> 2); // remove 2 MSB
        l2 |= (2L << 62); // set 2 MSB to '10'
        return new UUID(l1, l2);
    } 

    
    private static UUID constructUUID(UUIDType type, byte[] uuidBytes){
        // first, ensure type is ok
        int b = uuidBytes[BYTE_OFFSET_TYPE] & 0xF; // clear out high nibble
        b |= type.raw() << 4;
        uuidBytes[BYTE_OFFSET_TYPE] = (byte) b;
        // second, ensure variant is properly set too
        b = uuidBytes[BYTE_OFFSET_VARIATION] & 0x3F; // remove 2 MSB
        b |= 0x80; // set as '10'
        uuidBytes[BYTE_OFFSET_VARIATION] = (byte) b;
        return uuid(uuidBytes);
    }


    public static UUID uuid(byte[] bytes){
        _checkUUIDByteArray(bytes, 0);
        long l1 = gatherLong(bytes, 0);
        long l2 = gatherLong(bytes, 8);
        return new UUID(l1, l2);
    }

    protected final static long gatherLong(byte[] buffer, int offset){
        long hi = ((long) _gatherInt(buffer, offset)) << 32;
        //long lo = ((long) _gatherInt(buffer, offset+4)) & MASK_LOW_INT;
        long lo = (((long) _gatherInt(buffer, offset+4)) << 32) >>> 32;
        return hi | lo;
    }

    private final static int _gatherInt(byte[] buffer, int offset){
        return (buffer[offset] << 24) | ((buffer[offset+1] & 0xFF) << 16)
            | ((buffer[offset+2] & 0xFF) << 8) | (buffer[offset+3] & 0xFF);
    }

    private final static void _checkUUIDByteArray(byte[] bytes, int offset){
        if (bytes == null) {
            throw new IllegalArgumentException("Invalid byte[] passed: can not be null");
        }
        if (offset < 0) {
            throw new IllegalArgumentException("Invalid offset ("+offset+") passed: can not be negative");
        }
        if ((offset + 16) > bytes.length) {
            throw new IllegalArgumentException("Invalid offset ("+offset+") passed: not enough room in byte array (need 16 bytes)");
        }
    }

    private final static void _appendInt(int value, byte[] buffer, int offset){
        buffer[offset++] = (byte) (value >> 24);
        buffer[offset++] = (byte) (value >> 16);
        buffer[offset++] = (byte) (value >> 8);
        buffer[offset] = (byte) value;
    }

    private static UUID fromString(String id){
        if (id == null) {
            throw new NullPointerException();
        }
        if (id.length() != 36) {
            throw new NumberFormatException("UUID has to be represented by the standard 36-char representation");
        }

        long lo, hi;
        lo = hi = 0;
        
        for (int i = 0, j = 0; i < 36; ++j) {
        	
            // Need to bypass hyphens:
            switch (i) {
            case 8:
            case 13:
            case 18:
            case 23:
                if (id.charAt(i) != '-') {
                    throw new NumberFormatException("UUID has to be represented by the standard 36-char representation");
                }
                ++i;
            }
            int curr;
            char c = id.charAt(i);

            if (c >= '0' && c <= '9') {
                curr = (c - '0');
            } else if (c >= 'a' && c <= 'f') {
                curr = (c - 'a' + 10);
            } else if (c >= 'A' && c <= 'F') {
                curr = (c - 'A' + 10);
            } else {
                throw new NumberFormatException("Non-hex character at #"+i+": '"+c
                        +"' (value 0x"+Integer.toHexString(c)+")");
            }
            curr = (curr << 4);

            c = id.charAt(++i);

            if (c >= '0' && c <= '9') {
                curr |= (c - '0');
            } else if (c >= 'a' && c <= 'f') {
                curr |= (c - 'a' + 10);
            } else if (c >= 'A' && c <= 'F') {
                curr |= (c - 'A' + 10);
            } else {
                throw new NumberFormatException("Non-hex character at #"+i+": '"+c
                        +"' (value 0x"+Integer.toHexString(c)+")");
            }
            if (j < 8) {
            	hi = (hi << 8) | curr;
            } else {
            	lo = (lo << 8) | curr;
            }
            ++i;
        }		
        return new UUID(hi, lo);
    }
}

/

CREATE OR REPLACE FUNCTION uuid_private_namespace RETURN VARCHAR2 
AS LANGUAGE JAVA NAME 'UUIDFactory.namespace() return java.lang.String';

/

CREATE OR REPLACE FUNCTION uuid3(name VARCHAR2) RETURN VARCHAR2 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuid3asString(java.lang.String) return java.lang.String';

/

CREATE OR REPLACE FUNCTION uuid3b(name VARCHAR2) RETURN RAW 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuid3asBytes(java.lang.String) return oracle.sql.RAW';

/

CREATE OR REPLACE FUNCTION uuid4 RETURN VARCHAR2 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuid4asString() return java.lang.String';

/

CREATE OR REPLACE FUNCTION uuid4b RETURN RAW 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuid4asBytes() return oracle.sql.RAW';

/


CREATE OR REPLACE FUNCTION uuid5(name VARCHAR2) RETURN VARCHAR2 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuid5asString(java.lang.String) return java.lang.String';

/

CREATE OR REPLACE FUNCTION uuid5b(name VARCHAR2) RETURN RAW 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuid5asBytes(java.lang.String) return oracle.sql.RAW';

/

CREATE OR REPLACE FUNCTION uuidtoraw(uuid VARCHAR2) RETURN RAW 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuidAsBytes(java.lang.String) return oracle.sql.RAW';

/

CREATE OR REPLACE FUNCTION rawtouuid(value RAW) RETURN VARCHAR2 
AS LANGUAGE JAVA NAME 'UUIDFactory.uuidFromBytes(oracle.sql.RAW) return java.lang.String';

/