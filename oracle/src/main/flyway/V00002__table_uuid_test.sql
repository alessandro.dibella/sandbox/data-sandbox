CREATE SEQUENCE uuid_name_seq START WITH 1;

/

CREATE TABLE uuid_test(
    idn NUMBER(10) NOT NULL,
    ids CHAR(36) NOT NULL,
    idb RAW(16) NOT NULL
);

/

CREATE UNIQUE INDEX uuid_test_idn_idx ON uuid_test(idn);

/

CREATE UNIQUE INDEX uuid_test_ids_idx ON uuid_test(ids);

/

CREATE UNIQUE INDEX uuid_test_idb_idx ON uuid_test(idb);

/

