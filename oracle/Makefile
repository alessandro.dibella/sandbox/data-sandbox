CURRENT_USER=$(shell id -u):$(shell id -g)	
SCRIPT_DIR?=$(CURDIR)
SRC_DIR?=$(CURDIR)/src
LIQUIBASE_DIR?=$(SRC_DIR)/main/liquibase
FLYWAY_DIR?=$(SRC_DIR)/main/flyway
TEST_DIR?=$(CURDIR)/test
OUTPUT_DIR?=$(CURDIR)/out

ORACLE_CLIENT_DOCKER_IMAGE_NAME:=aldib/oracle-client:latest
TAURUS_DOCKER_IMAGE_NAME:=aldib/taurus:latest

DB_IS_NOT_INITIALIZED=$(shell docker volume inspect oracle_oracle-data &> /dev/null; echo $$?)

SQLPLUS := docker run --network db-sandbox --rm -it -v $(SCRIPT_DIR):/project $(ORACLE_CLIENT_DOCKER_IMAGE_NAME) sqlplus /nolog
LIQUIBASE := docker run --network db-sandbox --rm -it -v $(SCRIPT_DIR):/project $(ORACLE_CLIENT_DOCKER_IMAGE_NAME) liquibase --username SANDBOX --password SANDBOX --url jdbc:oracle:thin:@db:1521/orclsndb.localdomain --driver oracle.jdbc.driver.OracleDriver
FLYWAY := docker run --network db-sandbox --rm -it -v $(SCRIPT_DIR):/project $(ORACLE_CLIENT_DOCKER_IMAGE_NAME) flyway -user=SANDBOX -password=SANDBOX -url=jdbc:oracle:thin:@db:1521/orclsndb.localdomain -driver=oracle.jdbc.driver.OracleDriver -baselineOnMigrate=true
TAURUS := docker run --network db-sandbox --rm -it -v $(SCRIPT_DIR):/project $(TAURUS_DOCKER_IMAGE_NAME)

# .DEFAULT_GOAL := dist

define build-docker	
	@echo "Building docker image $(1) from $(2)"                                                                
	@cd $(SRC_DIR)/main/docker && DOCKER_BUILDKIT=1 docker build --tag $(1) -f $(notdir $(2)) . 	
	@docker image inspect $(1) | gzip > $(3)
endef


$(OUTPUT_DIR):
	@mkdir -p  $@

$(OUTPUT_DIR)/aldib-oracle-client.json.gz: $(SRC_DIR)/main/docker/Dockerfile.oracleclient $(OUTPUT_DIR)
	$(call build-docker,$(ORACLE_CLIENT_DOCKER_IMAGE_NAME), $<,$@)	

$(OUTPUT_DIR)/aldib-taurus.json.gz: $(SRC_DIR)/main/docker/Dockerfile.taurus $(OUTPUT_DIR)
	$(call build-docker,$(TAURUS_DOCKER_IMAGE_NAME), $<,$@)		

$(OUTPUT_DIR)/flyway_report.txt.gz: $(OUTPUT_DIR)/aldib-oracle-client.json.gz $(wildcard $(FLYWAY_DIR)/*.sql) $(OUTPUT_DIR)
	time $(FLYWAY) -locations=/project/src/main/flyway/ migrate | tee /dev/tty | gzip --stdout > $(OUTPUT_DIR)/flyway_report.txt.gz


start-db : $(OUTPUT_DIR)/aldib-oracle-client.json.gz
	docker-compose --file $(SCRIPT_DIR)/docker-compose.yml up --detach
ifneq ($(DB_IS_NOT_INITIALIZED),0)
	@TNS_ADMIN=$(SCRIPT_DIR)/tnsnames.ora; \
	for i in 1 2 3 4 5; do  \
		echo "Attempting to initialize the database in 30 seconds ($$i/5)";  \
		sleep 30;  \
		$(SQLPLUS) @/project/src/main/sql/setup.sql && break;  \
	done
endif		

stop-db:
	docker-compose --file $(SCRIPT_DIR)/docker-compose.yml down

migrate-db: $(OUTPUT_DIR)/flyway_report.txt.gz
	
generate-uuid-test-data: migrate-db
	time $(SQLPLUS) @/project/src/main/sql/generate_uuid_test_data.sql

run-uuid-test: $(OUTPUT_DIR)/aldib-taurus.json.gz $(OUTPUT_DIR)/flyway_report.txt.gz
	rm -rf $(OUTPUT_DIR)/uuid_load_test*.csv $(OUTPUT_DIR)/jmeter.log; \
	time $(TAURUS) /project/src/test/taurus/UUID_test_plan.jmx ; \
	time $(SQLPLUS) @/project/src/main/sql/rebuild_indexes.sql
	

clean: 
	@rm -rf $(OUTPUT_DIR)

clean-db: 
	docker-compose --file $(SCRIPT_DIR)/docker-compose.yml rm --stop --force -v
	docker volume rm oracle_oracle-data
	docker network rm db-sandbox
	rm -rf $(OUTPUT_DIR)/flyway_report.txt.gz

clean-all: clean clean-db

